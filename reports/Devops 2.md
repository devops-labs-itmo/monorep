# Devops 2

### 1. Инициализация роли

`ansible-galaxy init docker-install`

### 2. Конвертация плейбука в роль

```yml
---
- name: Make sure that old versions is uninstalled
  apt:
    name:
      - docker
      - docker-engine
      - docker.io
      - containerd
      - runc
    state: absent

- name: Install packages to allow apt to use a repository over HTTPS
  apt:
    name:
      - ca-certificates
      - curl
      - gnupg
      - lsb-release
    state: present

- name: Create directory for gpg key
  file:
    path: /etc/apt/keyrings
    state: directory

- name: Sign key by gpg
  shell:
    cmd: curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg

- name: Add docker repository
  apt_repository:
    repo: deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu focal stable

- name: Install docker package
  apt:
    update_cache: true
    name:
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-compose-plugin
```

### 3. Параметризовать роль через переменные

```yml
---
docker_gpg_url: https://download.docker.com/linux/ubuntu/gpg
docker_repo_url: https://download.docker.com/linux/ubuntu
docker_gpg_dir: /etc/apt/keyrings/
docker_gpg_filename: docker.gpg
```

```yml
- name: Create directory for gpg key
  file:
    path: {{ docker_gpg_dir }}
    state: directory

- name: Sign key by gpg
  shell:
    cmd: curl -fsSL {{ docker_gpg_url }} | sudo gpg --batch --yes --dearmor -o {{ docker_gpg_dir }}{{ docker_gpg_filename }}
- name: Add docker repository
  apt_repository:
    repo: deb [arch=amd64 signed-by={{ docker_gpg_dir }}{{ docker_gpg_filename }}] {{ docker_repo_url }} focal stable
```

### 4. Создать в gitlab группу для docker роли

1. `git init`

2. `git add *`

3. `git commit -m "Initial commit"`

4. `git remote add origin git@gitlab.com:devops-labs-itmo/docker-install-role.git`

5. `git push --set-upstream origin master`

### 5. Создать файл requirements.yml

```yml
- src: git+https://gitlab.com/devops-labs-itmo/docker-install-role
  name: devops_labs.docker-install
```

### 6. Запустить приложение

```yml
---
- name: Lab2 - Install docker
  hosts: all
  become: true
  roles:
    - devops_labs.docker-install

- name: Lab1 - Import django
  hosts: app
  tags: app

  tasks:
    - name: Make directory for project
      file:
        dest: /home/vagrant/project
        state: directory
    - name: Clone repo
      git:
        repo: https://github.com/mdn/django-locallibrary-tutorial
        dest: /home/vagrant/project
    - name: Install pip
      apt:
        name: pip
        state: present
      become: true
    - name: Install requirments
      pip:
        requirements: /home/vagrant/project/requirements.txt
    - name: Run project
      shell:
        cmd: |
          python3 manage.py makemigrations &&
          python3 manage.py migrate &&
          echo yes | python3 manage.py collectstatic &&
          python3 manage.py test &&
          echo | python3 manage.py createsuperuser &&
          nohup python3 manage.py runserver &
        chdir: /home/vagrant/project
```

`ansible-galaxy install -r requirments.yml`

`ansible-playbook lab_2_playbook.yml -i inventory.ini`
