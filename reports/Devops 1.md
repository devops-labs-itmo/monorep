# Devops 1

### 1. Установка Ansible

`yay -S ansible`

### 2. Установка vagrant

`yay -S vagrant`

### 3. Поднимаем vagrant

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"

  # Количество требуемых машин
  SERVERS = 5
  # Имя сетевого интерфейса для организации моста
  BRIDGE = "wlan0"
  SSH_PUB_KEY = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip

  def create_host(config, hostname, ip)
    config.vm.define hostname do |host|
      host.vm.network "private_network", ip: ip
      host.vm.network "public_network", bridge: BRIDGE
      host.vm.hostname = hostname
      host.vm.provision "shell", inline: "apt-get update && apt-get install -y python3-minimal"
      # Add my ssh key 
      host.vm.provision 'shell', inline: 'mkdir -p /root/.ssh'
      host.vm.provision 'shell', inline: "echo #{SSH_PUB_KEY} >> /root/.ssh/authorized_keys"
      host.vm.provision 'shell', inline: "echo #{SSH_PUB_KEY} >> /home/vagrant/.ssh/authorized_keys", privileged: false
      yield host if block_given?
    end
  end

  (1..SERVERS).each do |machine_id|
    create_host(config, "srv#{machine_id}", "192.168.56.#{200+machine_id}")
  end
end
```

Исходный Vagrantfile + 

- Изменен Bridge чтобы не выбирать интерфейс постоянно при поднятии машин

- Добавлен собственный ssh ключ

- Изменен пакет питона `python-minimal` -> `python3-minimal`

Делаем `vagrant up`

### 4. Inventory файл

```
srv1 ansible_host=192.168.56.201 ansible_user=vagrant
srv2 ansible_host=192.168.56.202 ansible_user=vagrant
srv3 ansible_host=192.168.56.203 ansible_user=vagrant

[app]
srv4 ansible_host=192.168.56.204 ansible_user=vagrant
srv5 ansible_host=192.168.56.205 ansible_user=vagrant
```

### 5. Playbook для установки Docker

```yml
---
- name: Lab1 - Install docker # https://docs.docker.com/engine/install/ubuntu/
  hosts: all
  become: true

  tasks:
    - name: Make sure that old versions is unintalled
      apt:
        name:
          - docker
          - docker-engine
          - docker.io
          - containerd
          - runc
        state: absent
    - name: Install packages to allow apt to use a repository over HTTPS
      apt:
        name:
          - ca-certificates
          - curl
          - gnupg
          - lsb-release
        state: present
    - name: Create directory for gpg key
      file:
        path: /etc/apt/keyrings
        state: directory
    - name: Sign key by gpg
      shell:
        cmd: curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg
    - name: Add docker repository
      apt_repository:
        repo: deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu focal stable
    - name: Install docker package
      apt:
        update_cache: true
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-compose-plugin
```

# 6-7. Запуск приложения

```yml
- name: Lab1 - Import django
  hosts: app
  tags: app

  tasks:
    - name: Make directory for project
      file:
        dest: /home/vagrant/project
        state: directory
    - name: Clone repo
      git:
        repo: https://github.com/mdn/django-locallibrary-tutorial
        dest: /home/vagrant/project
    - name: Install pip
      apt:
        name: pip
        state: present
      become: true
    - name: Install requirments
      pip:
        requirements: /home/vagrant/project/requirements.txt
    - name: Run project
      shell:
        cmd: |
          python3 manage.py makemigrations &&
          python3 manage.py migrate &&
          echo yes | python3 manage.py collectstatic &&
          python3 manage.py test &&
          echo | python3 manage.py createsuperuser &&
          nohup python3 manage.py runserver &
        chdir: /home/vagrant/project
```

```
vagrant@srv5:~/project$ ss -tulnp
Netid      State       Recv-Q      Send-Q                   Local Address:Port             Peer Address:Port      Process
udp        UNCONN      0           0                        127.0.0.53%lo:53                    0.0.0.0:*
udp        UNCONN      0           0                     10.0.2.15%enp0s3:68                    0.0.0.0:*
udp        UNCONN      0           0                192.168.50.106%enp0s9:68                    0.0.0.0:*
tcp        LISTEN      0           10                           127.0.0.1:8000                  0.0.0.0:*          users:(("python3",pid=36258,fd=4))
tcp        LISTEN      0           4096                     127.0.0.53%lo:53                    0.0.0.0:*
tcp        LISTEN      0           128                            0.0.0.0:22                    0.0.0.0:*
tcp        LISTEN      0           128                               [::]:22                       [::]:*
```
