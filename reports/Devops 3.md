# Devops 3

### 1. Поднимаем vagrant

`vagrant up`

### 2. Роль для установки nginx

###### handlers

```yml
---
- name: Restart nginx
  become: true
  service:
    name: nginx
    state: restarted
```

###### tasks

```yml
---
- name: Install nginx
  apt:
    name: nginx
    state: present

- name: Copy nginx conf to server
  copy:
    src: nginx.conf
    dest: /etc/nginx/

- name: Configure django site conf
  template:
    src: django_site.conf.j2
    dest: /etc/nginx/sites-enabled/django_site.conf

- name: Create dir for upstreams
  file: 
    path: /etc/nginx/include
    state: directory

- name: Configure upstreams
  template:
    src: upstreams.conf.j2
    dest: /etc/nginx/include/upstreams.conf
  notify: Restart nginx

```

###### templates

`django_site.conf.j2`

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name {{ server_name }};

    #root /var/www/example.com;
    #index index.html;

    location /static/ {
        alias /home/vagrant/project/staticfiles/;
    }

    location / {
        proxy_pass http://upstream_app;
        proxy_set_header Host $host;
    }
}
```

`upstreams.conf.j2`

```jinja2
upstream upstream_app {
    {% for server in nginx_upstreams %}
    server {{ server }}:8000;
    {% endfor %}
}
```

###### files

`nginx.conf`

```nginx
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
}

http {

	##
	# Basic Settings
	##

	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	##
	# SSL Settings
	##

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	##
	# Logging Settings
	##
	log_format upstreamlog '[$time_local] remote_addr: $remote_addr - remote_user: $remote_user -serv_name $server_name host: $host to: upstream_addr: $upstream_addr: $request $status upstream_response_time $upstream_response_time msec $msec request_time $request_time';

	access_log /var/log/nginx/access.log upstreamlog;
	error_log /var/log/nginx/error.log;

	##
	# Gzip Settings
	##

	gzip on;

	##
	# Virtual Host Configs
	##

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
	include /etc/nginx/include/*;
}
```

###### vars

```yml
---
nginx_upstreams: "{{ groups['app'] | map('extract', hostvars, ['ansible_host']) | union([]) }}"
server_name: "{{ ansible_host }}"
```

### 3. Playbook

```yml
---
- name: Lab3 - Install nginx
  hosts: web
  tags: [web, web-install]
  become: true
  roles:
    - nginx-install

- name: Lab3 - Create static files
  hosts: web
  tags: [web, static]
  vars:
    project_path: /home/vagrant/project
  tasks:
    - name: Make directory for project
      file:
        dest: "{{ project_path }}"
        state: directory
    - name: Clone repo
      git:
        repo: https://github.com/mdn/django-locallibrary-tutorial
        dest: "{{ project_path }}"
    - name: Install pip
      apt:
        name: pip
        state: present
      become: true
    - name: Install requirments
      pip:
        requirements: "{{ project_path }}/requirements.txt"
    - name: Fix manage.py
      file: dest="{{ project_path }}/manage.py" mode=a+x
    - name: Fix python bin path
      file:
        path: /usr/bin/python
        src: /usr/bin/python3
        state: link
      become: true
    - name: Generate static
      django_manage:
        command: collectstatic
        chdir: "{{ project_path }}"


- name: Lab3 - Install docker
  hosts: app
  tags: app
  become: true
  roles:
    - devops_labs.docker-install

- name: Lab3 - Download django container
  hosts: app
  tags: app
  become: true
  vars:
    django_container: timurbabs/django
  tasks:
    - name: Pull docker image
      community.docker.docker_image:
        name: "{{ django_container }}"
        source: pull

    - name: Start django container
      community.docker.docker_container:
        name: django
        image: "{{ django_container }}"
        ports:
          - "8000:8000"
        state: started

```

### Схема

<img src="Devops%203-assets/2023-01-22-21-41-44-image.png" title="" alt="" data-align="center">
