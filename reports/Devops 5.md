# Devops 5

### 1. Playbook
``` yml
---
- name: Lab5 - Install postgres
  hosts: db
  roles:
    - postgres-install
```

### 2. Roles
#### 2.0 Main
``` yml
- name: Install postgres
  import_tasks:
    install.yml
  tags: always

- name: Change data directory
  import_tasks:
    change_data_dir.yml
  when: postgres_custom_data_dir != ""

- name: Configure master
  import_tasks:
    configure_master.yml
  tags: master
  when: master

- name: Configure replicas
  import_tasks:
    configure_replica.yml
  tags: replica
  when: not master
```
#### 2.1 Install
``` yml
---
- name: Install postgres package and dependencies
  package:
    name: 
      - "{{ postgres_package_name }}"
      - python3-psycopg2
      - acl
    state: present
  become: true
```
#### 2.2 Change data directory
``` yml
- name: Create data directory
  file:
    path: "{{ postgres_custom_data_dir }}"
    state: directory
    owner: postgres
    group: postgres
    mode: 0700
  become: true


- name: Check if database already present
  find:
    path: "{{ postgres_custom_data_dir }}/"
  register: custom_data_dir_files
  become: true
- name: Init database
  shell:
    cmd: "{{ initdb_bin_path }} -D {{ postgres_custom_data_dir }}"
  when: custom_data_dir_files.matched == 0 and master
  become: true
  become_user: postgres

- name: Change postgres config
  lineinfile:
    state: present
    backrefs: true
    dest: "{{ postgres_conf_path }}"
    regexp: "data_directory = .*"
    line: "data_directory = '{{ postgres_custom_data_dir }}'"
  become: true
  notify: Restart postgres
  when: master
```
#### 2.3 Configure master
``` yml
- name: Change postgres config
  import_tasks:
    change_postgres_conf.yml

- name: Template pg_hba.conf
  import_tasks:
    template_pg_hba.yml

- name: Restart postgres
  import_tasks:
    restart_postgres.yml

- name: Create replication user account
  postgresql_user:
    name: "{{ pg_replication_user }}"
    password: "{{ pg_replication_user_pass }}"
    role_attr_flags: replication
  become: true
  become_user: postgres
```
#### 2.4 Configure replica
``` yml
- name: Change postgres config
  import_tasks:
    change_postgres_conf.yml

- name: Template pg_hba.conf
  import_tasks:
    template_pg_hba.yml

- name: Save replication user creds
  template:
    src: pgpass.j2
    dest: /var/lib/postgresql/.pgpass
    mode: 0600
    owner: postgres
    group: postgres
  become: true

- name: Get master ip
  set_fact:
    master_ip: "{{ hostvars[master_host]['ansible_host'] }}"
  vars: 
    - master_host: "{{ groups['db-master'][0] }}"

- name: Set database data directory
  set_fact:
    db_data_dir: "{{ postgres_custom_data_dir | default('/var/lib/postgresql/12/main/', true) }}"

- name: Check if database already initialized
  find:
    path: "{{ db_data_dir }}/"
  register: custom_data_dir_files
  become: true

- name: Init db
  shell:
    cmd: "pg_basebackup -c fast -X stream -h {{ master_ip }} -U {{ pg_replication_user }} -D {{ db_data_dir }}"
  become: true
  become_user: postgres
  when: custom_data_dir_files.matched == 0
```
### 3 Configs
#### 3.1 pg_hba.conf
``` jinja2
# TYPE  DATABASE        USER            ADDRESS                 METHOD

{% for connection in pg_hba_conf %}
# {{ connection.comment }}
{{ connection.type }}	{{ connection.database }}		{{ connection.role }}		{{ connection.address }}		{{ connection.method }}
{% endfor %}
```
#### 3.2 pgpass
`*:*:*:{{ pg_replication_user }}:{{ pg_replication_user_pass }}`
### 4 Group vars
#### db
``` yml
postgres_package_name: "postgresql-{{ postgres_version }}"
postgres_version: 12

postgres_configs_dir: "/etc/postgresql/{{ postgres_version }}/main"
postgres_conf_path: "{{ postgres_configs_dir }}/postgresql.conf"
pg_hba_path: "{{ postgres_configs_dir }}/pg_hba.conf"

postgres_custom_data_dir: "" # "/home/vagrant/tmp"
initdb_bin_path: "/usr/lib/postgresql/{{ postgres_version }}/bin/initdb"

pg_conf_settings:
  wal_level: replica
  max_wal_senders: 2
  wal_keep_segments: 100
  listen_addresses: "0.0.0.0"

pg_conf_regexps:
  - regexp: '#?wal_level = \w+(\s+#.*)'
    line: 'wal_level = {{ pg_conf_settings.wal_level }}'
  - regexp: '#?max_wal_senders = \d+(\s+#.*)'
    line: 'max_wal_senders = {{ pg_conf_settings.max_wal_senders }}'
  - regexp: '#?wal_keep_segments = .*(\s+#.*)'
    line: 'wal_keep_segments = {{ pg_conf_settings.wal_keep_segments }}'
  - regexp: "#listen_addresses = 'localhost'.*"
    line: "listen_addresses = '{{ pg_conf_settings.listen_addresses }}'"
  
pg_hba_conf:
  - { type: local, database: all, role: all, address: "", method: "peer", comment: '"local" is for Unix domain socket connections only' }
  - { type: host, database: all, role: all, address: "0.0.0.0/0", method: md5, comment: "" }
  - { type: local, database: replication, role: all, address: "", method: "peer", comment: "" }
  - { type: host, database: replication, role: all, address: "0.0.0.0/0", method: md5, comment: "" }

pg_replication_user: replica_guy
pg_replication_user_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          65383038323430313036313162333235633932343036353736323039343165393637353832373662
          6635303039346362643138353739653461633665623966310a316132623861383430326237363462
          31616231636230383534346464653735383663326361313238356361303430663139343230383630
          6266376339396466650a333833356130326636366637383633363137643061363166386438643332
          63313338383531366630343262666432613137653335386238363330663064343532
```
#### db-master
``` yml
master: true
```
#### db-replica
``` yml
master: false
postgres_custom_data_dir: /replica
```