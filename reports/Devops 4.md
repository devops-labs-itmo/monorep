# Devops 4

### 1. Playbook
``` yml
---
- name: Lab4 - Install open vpn
  hosts: all
  roles:
    - ovpn
```
### 2. Roles
#### 2.0 Main
``` yml
---
- name: Install packages
  package:
    name: 
      - openvpn
      - easy-rsa
    state: present
  become: true

- name: Run common checks
  import_tasks: common.yml
  tags: [server_config, client_files]

- name: Configure ovpn server
  import_tasks: server_configuration.yml
  tags: server_config

- name: Generate client connect files
  import_tasks: generate_client.yml
  tags: client_files
```
#### 2.1 Common
``` yml
- name: Create directory for easy-rsa configs
  file:
    path: "{{ easy_rsa_dir }}"
    state: directory
- name: Find all files in easy-rsa config directory
  find:
    paths: /usr/share/easy-rsa
    file_type: any
  register: global_easy_rsa_files
- name: Create symlinks to /usr/local/bin
  file:
    src: "{{ item.path }}"
    path: "{{ easy_rsa_dir }}/{{ item.path | basename }}"
    state: link
  loop: "{{ global_easy_rsa_files.files }}"
- name: Copy easy-rsa vars
  template:
    src: easy-rsa-vars
    dest: "{{ easy_rsa_dir }}/vars"
- name: Check which PKI files already exists
  stat:
    path: "{{ item }}"
  register: pki_all_check
  loop: "{{ cert_files_check }}"
- name: "Convert pki_all_check to dict {component_name: exists (true/false)}"
  set_fact:
    pki_components: "{{ pki_components | default({}) | combine({ item.item: not item.stat.exists }) }}"
  loop: "{{ pki_all_check.results }}"
```
#### 2.2 Server configuration
``` yml
- name: Create certificates
  import_tasks: create_certs.yml

- name: Template server.conf
  template:
    src: server.conf.j2
    dest: /etc/openvpn/server/server.conf
  become: true

- name: Enable port forwarding
  sysctl:
    name: net.ipv4.ip_forward
    value: '1'
    sysctl_set: yes
    state: present
    reload: yes
  become: true

- name: Enable ovpn service
  service:
    name: openvpn-server@server
    state: started
  become: true
```
#### 2.3 Client configuration
``` yml
- name: Create directory for client certificates
  file:
    path: "{{ client_cert_dir }}"
    state: directory

- name: Generate client certificate
  import_tasks: gen_req.yml
  vars:
    cert_name: "{{ client_name }}"
  when: pki_components[client_gen_req_check]

- name: Sign client certificate
  import_tasks: sign_req.yml
  vars:
    cert_name: "{{ client_name }}"
  when: pki_components[client_signed_cert_check]

- name: Template client config
  template:
    src: client.conf.j2
    dest: "{{ client_base_config }}"

- name: Collect all config files and certificates
  slurp:
    src: "{{ item }}"
  register: client_all_files_certs
  loop:
    - "{{ client_base_config }}"
    - "{{ pki_build_ca_check }}"
    - "{{ client_signed_cert_check }}"
    - "{{ client_gen_req_check }}"
    - "{{ ta_key_check }}"
- name: Parse client_all_files_certs
  set_fact:
    client_all_files_certs_list: "{{ client_all_files_certs_list | default({}) | combine({ item.source: item.content | b64decode }) }}"
  loop: "{{ client_all_files_certs.results }}"
- name: Concatenate all things to one big client config
  template:
    src: client_final_config.j2
    dest: "{{ playbook_dir }}/{{ client_name }}.ovpn"
  delegate_to: localhost
```
### 3. Configs
#### client_final_config.ovpn
``` jinja2
{{ client_all_files_certs_list[client_base_config] }}
<ca>
{{ client_all_files_certs_list[pki_build_ca_check] }}</ca>
<cert>
{{ client_all_files_certs_list[client_signed_cert_check] }}</cert>
<key>
{{ client_all_files_certs_list[client_gen_req_check] }}</key>
<tls-crypt>
{{ client_all_files_certs_list[ta_key_check] }}</tls-crypt>
```
#### easy-rsa-vars
```
set_var EASYRSA_ALGO "ec"
set_var EASYRSA_DIGEST "sha512"
set_var EASYRSA_REQ_COUNTRY    "US"
set_var EASYRSA_REQ_PROVINCE   "NewYork"
set_var EASYRSA_REQ_CITY       "New York City"
set_var EASYRSA_REQ_ORG        "DigitalOcean"
set_var EASYRSA_REQ_EMAIL      "admin@example.com"
set_var EASYRSA_REQ_OU         "Community"
```
### 4. Vars
``` yml
---
client_name: vlad_test

server_port: 1194
server_proto: udp

easy_rsa_dir: "{{ ansible_env.HOME }}/easy-rsa"
ovpn_server_cn: "server"

pki_dir: "{{ easy_rsa_dir }}/pki"
pki_gen_req_check: "{{ pki_dir }}/private/{{ ovpn_server_cn }}.key"
pki_build_ca_check: "{{ pki_dir }}/ca.crt"
pki_dh_keys_check: "{{ pki_dir }}/dh.pem"
pki_signed_cert_check: "{{ pki_dir }}/issued/{{ ovpn_server_cn }}.crt"
ta_key_check: "{{ easy_rsa_dir }}/ta.key"

client_cert_dir: "{{ easy_rsa_dir }}/client"
client_gen_req_check: "{{ pki_dir }}/private/{{ client_name }}.key"
client_signed_cert_check: "{{ pki_dir }}/issued/{{ client_name }}.crt"
client_base_config: "{{ client_cert_dir }}/client.conf"
client_check_files: 
  - "{{ client_gen_req_check }}"
  - "{{ client_signed_cert_check }}"

cert_files:
  - "{{ pki_gen_req_check }}"
  - "{{ pki_build_ca_check }}"
  - "{{ pki_dh_keys_check }}"
  - "{{ pki_signed_cert_check }}"
  - "{{ ta_key_check }}"

cert_files_check: "{{ cert_files +
 [ pki_dir ] + 
 client_check_files }}"
 ```
 ### 5. Molecule tests
 ``` yml
 ---
- name: Verify
  hosts: all
  vars_files: vars.yml
  tasks:
    - name: Example assertion
      ansible.builtin.assert:
        that: true

    - name: Collect fact about services
      service_facts:
    - name: Verify demon is running
      assert:
        that: ansible_facts['services']['openvpn-server@server.service']['state'] == 'running'

    - name: Gather facts on listening ports
      listen_ports_facts:
      become: true    
    - name: Find ovpn port
      set_fact:
        server_port_assert: "{{ ansible_facts | json_query(query) }}"
      vars:
        query: "udp_listen[?port==`{{ server_port }}`]"
    - name: Check vpn port parameters
      assert:
        that: 
          - server_port_assert[0].address == "0.0.0.0"
          - server_port_assert[0].name == "openvpn"
          - server_port_assert[0].user == "nobody"
```
