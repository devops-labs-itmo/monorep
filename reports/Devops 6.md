# Devops 6

### 1. Playbook

```yml
---
- name: Lab6 - Install and run app
  hosts: app
  vars:
    - app_dir: /home/vagrant/app
  tasks:
    - name: Install java
      package:
        name: default-jdk
        state: present
      become: true
    - name: Create directory for app
      file:
        path: "{{ app_dir }}"
        state: directory 
    - name: Clone git repo
      git:
        repo: https://github.com/grafana/spring-boot-demo
        dest: "{{ app_dir }}"
    - name: Run app
      shell:
        cmd: nohup ./gradlew :bootRun &
        chdir: "{{ app_dir }}"
- name: Lab6 - Install docker
  hosts: monitoring
  roles: 
    - devops_labs.docker-install
  become: true
- name: Lab6 - Install grafana, prometheus, loki
  hosts: monitoring
  roles: 
    - grafana
    - loki
    - prometheus
```

### 2. Roles

#### 2.1 Grafana

```yml
---
- name: Pull grafana image
  community.docker.docker_image:
    name: "grafana/grafana-oss:latest"
    source: pull
  become: true

- name: Start grafana container
  community.docker.docker_container:
    name: grafana
    image: "grafana/grafana-oss:latest"
    ports:
      - "3000:3000"
    state: started
  become: true
```

#### 2.2 Loki

```yml
---
- name: Pull loki image
  community.docker.docker_image:
    name: "grafana/loki:latest"
    source: pull
  become: true

- name: Start loki container
  community.docker.docker_container:
    name: loki
    image: "grafana/loki:latest"
    ports:
      - "3100:3100"
    state: started
  become: true
```

#### 2.3 Prometheus

##### task

```yml
---
- name: Pull prometheus image
  community.docker.docker_image:
    name: "prom/prometheus:latest"
    source: pull
  become: true

- name: Get app host ip
  set_fact:
    app_ip: "{{ hostvars[app_host]['ansible_host'] }}"
  vars: 
    - app_host: "{{ groups['app'][0] }}"

- name: Template prometheus config
  template:
    src: prometheus.yml.j2
    dest: /home/vagrant/prometheus.yml

- name: Start prometheus container
  community.docker.docker_container:
    name: prometheus
    image: "prom/prometheus:latest"
    ports:
      - "9090:9090"
    mounts:
      - target: /etc/prometheus/prometheus.yml
        source: /home/vagrant/prometheus.yml
        type: bind
    state: started
  become: true
```

##### config

```yml
global:
  scrape_interval: 5s
  evaluation_interval: 30s
  scrape_timeout: 3s

scrape_configs:
  - job_name: app
    metrics_path: /actuator/prometheus
    scheme: http
    static_configs:
      - targets:
          - {{ app_ip }}:1235
```

### 3. Скрины

#### app

<img src="Devops%206-assets/2023-01-27-02-53-57-image.png" title="" alt="" data-align="center">

#### prometheus

<img src="Devops%206-assets/2023-01-27-02-54-49-image.png" title="" alt="" data-align="center">

#### grafana

##### dashboard

<img src="Devops%206-assets/2023-01-27-02-55-12-image.png" title="" alt="" data-align="center">

##### alerts

<img src="Devops%206-assets/2023-01-27-02-56-53-image.png" title="" alt="" data-align="center">
